from django.test import LiveServerTestCase, TestCase, tag
from django.urls import resolve
from django.test import TestCase , Client
from django.apps import apps
from .apps import HomepageConfig
from .views import index 


class UnitTestku(TestCase):

    def test_status(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_apps(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage') 

    def test_get(self):
            response = Client().get('/')
            html_kembalian = response.content.decode('utf8')
            self.assertIn("Biodata", html_kembalian)
               
           
    def test_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
